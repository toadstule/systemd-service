import glob

from setuptools import setup

VERSION = '0.9.1'

setup(
    name='systemd-service',
    version=VERSION,
    packages=['tests', 'systemd_service'],
    url='https://bitbucket.org/jibcode/systemd-service',
    license='LGPL',
    author='Steve Jibson',
    author_email='steve@jibson.com',
    description='Provides base class and tools for running a Python-based systemd service',
    scripts=glob.glob('scripts/*.py'),
)
