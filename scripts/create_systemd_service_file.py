#! python

import argparse

from systemd_service import SERVICE_FILE_TEMPLATE

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Create a systemd service file')

    parser.add_argument('--write', action='store_true', default=False,
                        help='Write the file in /etc/systemd/system/ (otherwise print to stdout)')

    parser.add_argument('--name', required=True, type=str,
                        help='Name of the service')

    parser.add_argument('--description', required=True, type=str,
                        help='Service description (Text string)')

    parser.add_argument('--exec-start', required=True, type=str,
                        help='The ExecStart command')

    parser.add_argument('--group', default='root',
                        help='Run to this group')

    parser.add_argument('--user', default='root',
                        help='Run as this user')

    parser.add_argument('--working-directory', type=str, default='/tmp',
                        help='Change to this working directory')

    args = parser.parse_args()

    if args.write:
        with open('/etc/systemd/system/{}.service'.format(args.name), 'w') as outfile:
            outfile.write(SERVICE_FILE_TEMPLATE.format(**vars(args)))
    else:
        print(SERVICE_FILE_TEMPLATE.format(**vars(args)))
