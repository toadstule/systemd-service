# systemd-service:

Base class and tools for running a Python-based systemd service.

## Notes

Beginning all of your project's service names with a prefix (i.e. myproj_publish, myproj_consume) allows them to be 
managed as a group with commands like "systemctl start myproj_*"


## License

This project is licensed under the LGPLv3 License - see the [LICENSE.md](LICENSE.md) file for details