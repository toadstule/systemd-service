from unittest import TestCase

import systemd_service


class MyService(systemd_service.Service):
    pass


class TestService(TestCase):
    def test__sig_hup(self):
        my_service = MyService()
        self.assertFalse(my_service._reload)

        my_service._sig_hup()
        self.assertTrue(my_service._reload)

    def test__sig_term(self):
        my_service = MyService()
        self.assertFalse(my_service._shutdown)

        my_service._sig_term()
        self.assertTrue(my_service._shutdown)
