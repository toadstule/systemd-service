import signal


class Service(object):
    """
    Service class
    """
    _reload = False
    _shutdown = False

    def __init__(self):
        signal.signal(signal.SIGTERM, self._sig_term)
        signal.signal(signal.SIGHUP, self._sig_hup)

    def _sig_hup(self, *_, **__):
        print('Received HUP signal')
        self._reload = True

    def _sig_term(self, *_, **__):
        print('Received TERM signal')
        self._shutdown = True
