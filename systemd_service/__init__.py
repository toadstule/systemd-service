from .service import Service

__all__ = ["Service"]

SERVICE_FILE_TEMPLATE = """
[Unit]
Description={description}
After=syslog.target

[Service]
Type=idle
User={user}
Group={group}
Environment="PYTHONUNBUFFERED=1"
WorkingDirectory={working_directory}
ExecStart={exec_start}
Restart=always

[Install]
WantedBy=multi-user.target
"""
